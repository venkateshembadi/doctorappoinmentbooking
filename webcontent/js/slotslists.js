/**
 * @description list of slot lists
 */
const slotsList = () => {
    let httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
    }
    let promise = new Promise(function (resolve, reject) {
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status != 200) {
                    reject(`Error, status code ${this.status}`);
                } else {
                    let tableEl = document.getElementsByTagName('table');
                    if (tableEl[0] !== undefined) {
                        tableEl[0].remove()
                    }
                    let table = document.createElement('table');

                    let tbody = document.createElement('tbody');

                    let thead = document.createElement('thead');

                    let tr = document.createElement('tr');

                    let td1 = document.createElement('td');
                    let thId = document.createTextNode('Id');
                    td1.append(thId);
                    td1.style.border = '1px solid #000000';
                    td1.style.backgroundColor = '1px solid #808080';

                    let td2 = document.createElement('td');
                    let thDemail = document.createTextNode('Doctor Email');
                    td2.append(thDemail);

                    td2.style.border = '1px solid #000000';
                    td2.style.backgroundColor = '1px solid #808080';

                    let td3 = document.createElement('td');
                    let thDate = document.createTextNode('Date');
                    td3.append(thDate);
                    td3.style.border = '1px solid #000000';

                    let td4 = document.createElement('td');
                    let thTime = document.createTextNode('Time');
                    td4.append(thTime);
                    td4.style.border = '1px solid #000000';


                    let td5 = document.createElement('td');
                    let thSpecialist = document.createTextNode('Specialist');
                    td5.append(thSpecialist);
                    td5.style.border = '1px solid #000000';

                    let td6 = document.createElement('td');
                    let thDocName = document.createTextNode('Doctor Name');
                    td6.append(thDocName);
                    td6.style.border = '1px solid #000000';

                    let td7 = document.createElement('td');
                    let thAction = document.createTextNode('Booking Action');
                    td7.append(thAction);
                    td7.style.border = '1px solid #000000';
                    table.align = 'center';
                    table.width = '1350px'

                    table.style.border = '5px solid #000000';



                    tr.append(td1);
                    tr.append(td2);
                    tr.append(td3);
                    tr.append(td4);
                    tr.append(td5);
                    tr.append(td6);
                    tr.append(td7);

                    thead.appendChild(tr);
                    table.appendChild(thead);
                    table.appendChild(tbody);


                    let data = JSON.parse(this.response);
                    let len = data.length;

                    if (len > 0) {
                        for (let i = 0; i < len; i++) {
                            let tableBody = document.createElement('tr');

                            let td1Body = document.createElement('td');
                            let textNode1 = document.createTextNode(data[i].id);
                            td1Body.append(textNode1);

                            let td2Body = document.createElement('td');
                            let textNode2 = document.createTextNode(data[i].doctorEmailId);
                            td2Body.append(textNode2);

                            let td3Body = document.createElement('td');
                            let textNode3 = document.createTextNode(data[i].slotDate);
                            td3Body.append(textNode3);

                            let td4Body = document.createElement('td');
                            let textNode4 = document.createTextNode(data[i].slotTime);
                            td4Body.append(textNode4);

                            let td5Body = document.createElement('td');
                            let textNode5 = document.createTextNode(data[i].specialist);
                            td5Body.append(textNode5);

                            let td6Body = document.createElement('td');
                            let textNode6 = document.createTextNode(data[i].doctorName);
                            td6Body.append(textNode6);

                            let td7Body = document.createElement('td');

                            let bookDoctorButton = document.createElement('button');
                            let bookDoctorButtonTextNode = document.createTextNode('Book Doctor');
                            bookDoctorButton.appendChild(bookDoctorButtonTextNode);
                            bookDoctorButton.addEventListener('click', function () {
                                let data = this.parentElement.parentElement.cells;
                                let obj1 = { id: data[0].innerHTML, doctorEmail: data[1].innerHTML, slotDate: data[2].innerHTML, slotTime: data[3].innerHTML, specialist: data[4].innerHTML, doctorName: data[5].innerHTML }
                                localStorage.setItem('doctorBooking', JSON.stringify(obj1));
                                window.location.assign('slotbooking.html');
                            });

                            td7Body.append(bookDoctorButton);

                            td1Body.style.border = '1px solid #000000';
                            td2Body.style.border = '1px solid #000000';
                            td3Body.style.border = '1px solid #000000';
                            td4Body.style.border = '1px solid #000000';
                            td5Body.style.border = '1px solid #000000';
                            td6Body.style.border = '1px solid #000000';
                            td7Body.style.border = '1px solid #000000';

                            tableBody.append(td1Body);
                            tableBody.append(td2Body);
                            tableBody.append(td3Body);
                            tableBody.append(td4Body);
                            tableBody.append(td5Body);
                            tableBody.append(td6Body);
                            tableBody.append(td7Body);
                            tbody.append(tableBody);
                        }
                    }
                    else {
                        let data = document.createElement('h4');
                        let noData = document.createTextNode('No data Found')
                        data.appendChild(noData);
                        tbody.appendChild(data);
                    }
                    let body = document.getElementsByTagName('body')[0];
                    body.appendChild(table);
                }
            }
        }
        const url = 'http://localhost:3000/slots';
        httpRequest.open('get', url, true);
        httpRequest.send();
    });

    promise.then((response) => {
        console.log(response);
    }).catch((error) => {
        console.log(error);
    })
}
/**
 * @description Patient Slot List
 */
const patientSlotsList = () => {

    let httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
    }
    let promise = new Promise(function (resolve, reject) {
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status != 200) {
                    reject(`Error, status code ${this.status}`);
                } else {
                    let tableEl = document.getElementsByTagName('table');
                    if (tableEl[0] !== undefined) {
                        tableEl[0].remove()
                    }
                    let table = document.createElement('table');

                    let tbody = document.createElement('tbody');

                    let thead = document.createElement('thead');

                    let tr = document.createElement('tr');

                    let td1 = document.createElement('td');
                    let thId = document.createTextNode('Id');
                    td1.append(thId);
                    td1.style.border = '1px solid #000000';
                    td1.style.backgroundColor = '1px solid #808080';

                    let td2 = document.createElement('td');
                    let thDemail = document.createTextNode('Doctor Email');
                    td2.append(thDemail);

                    td2.style.border = '1px solid #000000';
                    td2.style.backgroundColor = '1px solid #808080';

                    let td3 = document.createElement('td');
                    let thDate = document.createTextNode('Date');
                    td3.append(thDate);
                    td3.style.border = '1px solid #000000';

                    let td4 = document.createElement('td');
                    let thTime = document.createTextNode('Time');
                    td4.append(thTime);
                    td4.style.border = '1px solid #000000';


                    let td5 = document.createElement('td');
                    let thSpecialist = document.createTextNode('Specialist');
                    td5.append(thSpecialist);
                    td5.style.border = '1px solid #000000';

                    let td6 = document.createElement('td');
                    let thDocName = document.createTextNode('Doctor Name');
                    td6.append(thDocName);
                    td6.style.border = '1px solid #000000';
                    table.style.border = '5px solid #000000';
                    table.width = '1350px'
                    table.align = 'center';

                    tr.append(td1);
                    tr.append(td2);
                    tr.append(td3);
                    tr.append(td4);
                    tr.append(td5);
                    tr.append(td6);


                    thead.appendChild(tr);
                    table.appendChild(thead);
                    table.appendChild(tbody);


                    let data = JSON.parse(this.response);
                    let len = data.length;

                    if (len > 0) {
                        for (let i = 0; i < len; i++) {
                            let tableBody = document.createElement('tr');

                            let td1Body = document.createElement('td');
                            let textNode1 = document.createTextNode(data[i].id);
                            td1Body.append(textNode1);

                            let td2Body = document.createElement('td');
                            let textNode2 = document.createTextNode(data[i].doctorEmailId);
                            td2Body.append(textNode2);

                            let td3Body = document.createElement('td');
                            let textNode3 = document.createTextNode(data[i].slotDate);
                            td3Body.append(textNode3);

                            let td4Body = document.createElement('td');
                            let textNode4 = document.createTextNode(data[i].slotTime);
                            td4Body.append(textNode4);

                            let td5Body = document.createElement('td');
                            let textNode5 = document.createTextNode(data[i].specialist);
                            td5Body.append(textNode5);

                            let td6Body = document.createElement('td');
                            let textNode6 = document.createTextNode(data[i].doctorName);
                            td6Body.append(textNode6);

                            td1Body.style.border = '1px solid #000000';
                            td2Body.style.border = '1px solid #000000';
                            td3Body.style.border = '1px solid #000000';
                            td4Body.style.border = '1px solid #000000';
                            td5Body.style.border = '1px solid #000000';
                            td6Body.style.border = '1px solid #000000';


                            tableBody.append(td1Body);
                            tableBody.append(td2Body);
                            tableBody.append(td3Body);
                            tableBody.append(td4Body);
                            tableBody.append(td5Body);
                            tableBody.append(td6Body);
                            tbody.append(tableBody);
                        }
                    }
                    else {
                        let data = document.createElement('h4');
                        let noData = document.createTextNode('No data Found')
                        data.appendChild(noData);
                        tbody.appendChild(data);
                    }
                    let body = document.getElementsByTagName('body')[0];
                    body.appendChild(table);
                }
            }

        }
        const url = 'http://localhost:3000/slots'
        httpRequest.open('get', url, true);
        httpRequest.send();
    });

    promise.then((response) => {
        console.log(response);
    }).catch((error) => {
        console.log(error);
    })
}
/**
 * @description list of appoinment
 */
const appointmentList = () => {
    let httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
    }
    let promise = Promise(function (resolve, reject) {
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status != 200) {
                    reject(`Error, status code ${this.status}`);
                }
                else {

                    let tableEl = document.getElementsByTagName('table');
                    if (tableEl[0] !== undefined) {
                        tableEl[0].remove()
                    }
                    let table = document.createElement('table');

                    let tbody = document.createElement('tbody');

                    let thead = document.createElement('thead');

                    let tr = document.createElement('tr');

                    let td1 = document.createElement('td');
                    let thId = document.createTextNode('Id');
                    td1.append(thId);
                    td1.style.border = '1px solid #000000';
                    td1.style.backgroundColor = '1px solid #808080';

                    let td2 = document.createElement('td');
                    let thDemail = document.createTextNode('Doctor Email');
                    td2.append(thDemail);

                    td2.style.border = '1px solid #000000';
                    td2.style.backgroundColor = '1px solid #808080';

                    let td3 = document.createElement('td');
                    let thDate = document.createTextNode('Booking Date');
                    td3.append(thDate);
                    td3.style.border = '1px solid #000000';

                    let td4 = document.createElement('td');
                    let thTime = document.createTextNode('Time');
                    td4.append(thTime);
                    td4.style.border = '1px solid #000000';


                    let td5 = document.createElement('td');
                    let thSpecialist = document.createTextNode('Specialist');
                    td5.append(thSpecialist);
                    td5.style.border = '1px solid #000000';

                    let td6 = document.createElement('td');
                    let thPemail = document.createTextNode('Patient Email');
                    td6.append(thPemail);
                    td6.style.border = '1px solid #000000';

                    let td7 = document.createElement('td');
                    let thDoctoremail = document.createTextNode('Doctor Name');
                    td7.append(thDoctoremail);
                    td7.style.border = '1px solid #000000';

                    let td8 = document.createElement('td');
                    let thProblem = document.createTextNode('Patient Problem');
                    td8.append(thProblem);
                    td8.style.border = '1px solid #000000';


                    table.style.border = '5px solid #000000';
                    table.align = 'center';
                    table.width = '1350px'

                    tr.append(td1);
                    tr.append(td2);
                    tr.append(td3);
                    tr.append(td4);
                    tr.append(td5);
                    tr.append(td6);
                    tr.append(td7);
                    tr.append(td8);

                    thead.appendChild(tr);
                    table.appendChild(thead);
                    table.appendChild(tbody);


                    let data = JSON.parse(this.response);
                    let len = data.length;

                    if (len > 0) {
                        for (let i = 0; i < len; i++) {
                            let tableBody = document.createElement('tr');

                            let td1Body = document.createElement('td');
                            let textNode1 = document.createTextNode(data[i].id);
                            td1Body.append(textNode1);

                            let td2Body = document.createElement('td');
                            let textNode2 = document.createTextNode(data[i].doctorEmail);
                            td2Body.append(textNode2);

                            let td3Body = document.createElement('td');
                            let textNode3 = document.createTextNode(data[i].slotDate);
                            td3Body.append(textNode3);

                            let td4Body = document.createElement('td');
                            let textNode4 = document.createTextNode(data[i].slotTime);
                            td4Body.append(textNode4);

                            let td5Body = document.createElement('td');
                            let textNode5 = document.createTextNode(data[i].specialist);
                            td5Body.append(textNode5);

                            let td6Body = document.createElement('td');
                            let textNode6 = document.createTextNode(data[i].patientEmailId);
                            td6Body.append(textNode6);

                            let td7Body = document.createElement('td');
                            let textNode7 = document.createTextNode(data[i].doctorName);
                            td7Body.append(textNode7);

                            let td8Body = document.createElement('td');
                            let textNode8 = document.createTextNode(data[i].problem);
                            td8Body.append(textNode8);


                            td1Body.style.border = '1px solid #000000';
                            td2Body.style.border = '1px solid #000000';
                            td3Body.style.border = '1px solid #000000';
                            td4Body.style.border = '1px solid #000000';
                            td5Body.style.border = '1px solid #000000';
                            td6Body.style.border = '1px solid #000000';
                            td7Body.style.border = '1px solid #000000';
                            td8Body.style.border = '1px solid #000000';

                            tableBody.append(td1Body);
                            tableBody.append(td2Body);
                            tableBody.append(td3Body);
                            tableBody.append(td4Body);
                            tableBody.append(td5Body);
                            tableBody.append(td6Body);
                            tableBody.append(td7Body);
                            tableBody.append(td8Body);

                            tbody.append(tableBody);
                        }
                    }
                    else {
                        let data = document.createElement('h4');
                        let noData = document.createTextNode('No data Found')
                        data.appendChild(noData);
                        tbody.appendChild(data);
                    }
                    let body = document.getElementsByTagName('body')[0];
                    body.appendChild(table);
                }
            }
        }
        const url = 'http://localhost:3000/appointment';
        httpRequest.open('get', url, true);
        httpRequest.send();

    });
    promise.then((response) => {
        console.log(response);
    }).catch((error) => {
        console.log(error);
    })
}