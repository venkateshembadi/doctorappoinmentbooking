/**
 * @description User registration and validations
 */
const registerUser = () => {

    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;

    const mobileNumber = document.getElementById('mobileNumber').value;

    const role = document.getElementById('role').value;

    let password = Math.random().toString(36).slice(-8);
    console.log(password);

    const obj = { name, email, mobileNumber, role, password };
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const pattern = '[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)';

    if (name === '') {
        alert('Please enter your name.');
        name.focus();
        return false;
    }

    if (role === '') {
        alert('Please enter role.');
        role.focus();
        return false;
    }

    else if (email === '' || !email.match(mailformat) || !email.match(pattern)) {
        alert('Please enter a valid e-mail address.');
        email.focus();
        return false;
    }


    else if (mobileNumber === '' || mobileNumber.length != 10) {
        alert('Please enter a valid Mobile');
        mno.focus();
        return false;
    }


    let httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
    }
    let promise = new Promise(function (resolve, reject) {
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 201) {
                resolve('User Registered successfully');
            }
            else {
                reject(`Error, status code ${this.status}`);
            }
        }
        const url = 'http://localhost:3000/users'
        httpRequest.open('post', url, true);
        httpRequest.setRequestHeader('Content-type', 'application/json');
        httpRequest.send(JSON.stringify(obj));
    });
    promise.then((response) => {
        console.log(response);
    }).catch((error) => {
        console.log(error);
    })
}