/**
 * @description Check valid user is logging application 
 */
const validateUser = () => {
    const emailId = document.getElementById('email').value;
    const password = document.getElementById('pwd').value;
    const role = document.getElementById('role').value;

    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let pattern = '[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)';

    if (email === '' || !emailId.match(mailformat) || !emailId.match(pattern)) {
        window.alert('Please enter a valid e-mail address.');
        email.focus();
        return false;
    }
    else if (pwd === '') {
        window.alert('Please enter a valid pwd.');
        pwd.focus();
        return false;
    }
    else if (role === '') {
        window.alert('Please enter a valid role');
        role.focus();
        return false;
    }

    let httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
    }
    let promise = new Promise(function (resolve, reject) {
    httpRequest.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let data = JSON.parse(this.response);
/**
 * @description Based on user role redirecting to different page
 */
            let len = data.length;
            if (len > 0) {
                let role = data[0].role;
                sessionStorage.setItem('emailId', emailId);
                if (role ==='patient') {
                    sessionStorage.setItem('patientEmailId', emailId);
                    window.location.assign('patient.html');

                }
                else if (role ==='doctor') {
                    sessionStorage.setItem('doctorEmailId', emailId);
                    window.location.assign('doctor.html');
                }
                else {
                    alert('Invalid role type');
                }

            } else {
                reject(`Error, status code ${this.status}`);
            }
        }
        

    }
   
    const url = `http://localhost:3000/users?email=${emailId}&password=${password}&role=${role}`;
    console.log(url);
    httpRequest.open('get', url, true);
    httpRequest.send();

});
promise.then((response) => {
    console.log(response);
}).catch((error) => {
    console.log(error);
})
}


